let changePlaneButton = document.getElementById("changingPlans")
let changePlan = true

function onclickChangeBtn() {
    if (changePlan) {
        changePlaneButton.style.marginLeft = "auto"
        document.getElementById("basicPrice").textContent = 19.99
        document.getElementById("profPrice").textContent = 24.99
        document.getElementById("masterPrice").textContent = 39.99
        changePlan = false
    } else {
        changePlaneButton.style.marginLeft = "0px"
        document.getElementById("basicPrice").textContent = 199.99
        document.getElementById("profPrice").textContent = 249.99
        document.getElementById("masterPrice").textContent = 399.99
        changePlan = true
    }
}